export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'alibaba',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: `https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;600;800&display=swap`,
      },
    ],
  },

  // test
  publicRuntimeConfig: {
    baseURL: process.env.BASE_URL,
    apiBaseUrl: process.env.API_BASE_URL,
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ['@/assets/scss/custom.scss'],
  // Doc: https://github.com/nuxt-community/style-resources-module
  styleResources: {
    scss: ['./assets/scss/_variables.scss'],
  },

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [{ src: '~/plugins/fuse.js', ssr: true }],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/style-resources-module
    '@nuxtjs/style-resources',
    // Doc: https://color-mode.nuxtjs.org/
    '@nuxtjs/color-mode',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    baseURL: process.env.API_BASE_URL,
  },
  // sentry module configuration (https://github.com/nuxt-community/sentry-module)
  sentry: {
    dsn:
      process.env.NODE_ENV === 'production'
        ? 'https://34c4eb0eb66c458eb7ce43ff52f7f9fcc@sentry.io/18642688'
        : false,
    config: {}, // Additional config
  },

  // google tag manager (https://github.com/nuxt-community/gtm-module)
  gtm: {
    id: 'GTM-XXXXXXX',
    layer: 'dataLayer',
    pageTracking: true,
    respectDoNotTrack: false,
    dev: false, // set to false to disable in dev mode
  },

  // bootstrapVue configuration
  bootstrapVue: {
    bootstrapCSS: false,
    bootstrapVueCSS: false,
    components: [
      'BContainer',
      'BRow',
      'BCol',
      'BImgLazy',
      'BFormSelect',
      'b-aspect',
      'BFormInput',
    ],
    componentPlugins: [],
    directives: [],
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    // publicPath: 'https://cdn.esfahanahan.com',
    // analyze: true,
    // filenames: {
    //     chunk: () => '[name].js',
    //     css: () => '[name].css',
    //     img: () => '[path][name].[ext]',
    //     font: () => '[path][name].[ext]'
    // },
    extractCSS: true,
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true,
          },
        })
      }
    },
  },
}
