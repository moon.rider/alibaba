# alibaba
Demo Address: [See Demo](https://hungry-kirch-0350e3.netlify.app/)
## Config Setup
- Rename `.env.example` to `.env`
- Setup `sentry` and `google tag manager` in `nuxt.config.js`

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```
## Docker
```bash
# Frist install Docker and Docker-Compose 
$ docker-compose up --build -d
```
## TODO:

- Better use offline font 
- Add pagnation for api and show item by scroll(lazy load) or pagination
- Add query string to url for filter and sort and search
- Add animation to filter item


For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
