export const state = () => ({
  countriesList: [],
  countriesListFilter: [],
})

export const getters = {
  getCountriesList(state) {
    return state.countriesList
  },
  getCountriesListFilter(state) {
    return state.countriesListFilter
  },
  getCountry: (state) => (code) => {
    return state.countriesList.find((x) => x.alpha3Code === code.toUpperCase())
  },
  getBordersCountries: (state) => (Countries) => {
    return state.countriesList.filter((x) => {
      return ~Countries.indexOf(x.alpha3Code)
    })
  },
}

export const actions = {
  async fetchCountriesList({ commit }) {
    await this.$axios
      .$get('all')
      .then((response) => {
        commit('SET_COUNTRIES_LIST', response)
        commit('SET_COUNTRIES_LIST_FILTER', response)
      })
      .catch((error) => {
        throw new Error(error)
      })
  },
}

export const mutations = {
  SET_COUNTRIES_LIST(state, countries) {
    state.countriesList = countries
  },
  SET_COUNTRIES_LIST_FILTER(state, countries) {
    state.countriesListFilter = countries
  },
  SET_COUNTRIES_BY_REGION(state, region) {
    if (!region.region) {
      state.countriesListFilter = state.countriesList
    } else {
      state.countriesListFilter = state.countriesList.filter((x) => {
        return x.region === region.region
      })
    }
  },
  SET_COUNTRIES_BY_SEARCH(state, search) {
    // doc : https://fusejs.io/api/options.html
    this.$search(state.countriesList, search.value, {
      keys: ['name'],
    }).then((results) => {
      if (search.value) {
        state.countriesListFilter = results
      } else {
        state.countriesListFilter = state.countriesList
      }
    })

    // simple search without Fuzzy search
    // state.countriesListFilter = state.countriesList.filter((item) => {
    //   return item.name.toLowerCase().includes(search.value.toLowerCase())
    // })
  },
  SET_COUNTRIES_BY_SORT(state, sort) {
    if (sort.value === 'name') {
      state.countriesListFilter = state.countriesList.sort((x, y) =>
        x.name.localeCompare(y.name)
      )
    } else if (sort.value === 'population-asc') {
      state.countriesListFilter = state.countriesList.sort(
        (x, y) => x.population - y.population
      )
    } else {
      state.countriesListFilter = state.countriesList.sort(
        (x, y) => y.population - x.population
      )
    }
  },
}
