import Fuse from 'fuse.js'

export default ({ app }, inject) => {
  // Inject $hello(msg) in Vue, context and store.
  inject('search', (list, value, options) => {
    return new Promise((resolve, reject) => {
      const fuse = new Fuse(list, options)
      const results = fuse.search(value)
      resolve(results)
    })
  })
}
